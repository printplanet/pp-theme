<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    public function index()
    {
        return $this->json([
            'token' => 'jkhfp9347658g3fh937425630tg7t3gp932',
            'total' => 25.9,
            'currency' => 'EUR',
            'items_count' => 2,
        ]);
    }
}