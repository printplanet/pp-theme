<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;

class HomepageController extends AbstractController
{
    public function index($slug = null)
    {
        return $this->render('Homepage/index.html.twig', compact('slug'));
    }

    public function presentation()
    {
        $sku = "1004";
        $tsId = "XA2A8D35838AF5F63E5EB0E05847B1CB8";

        $apiUrl = "https://cdn1.api.trustedshops.com/shops/";

        $summaryUrl = $apiUrl . $tsId . "/products/skus/" . HomepageController::strToHex($sku) . "/productstickersummaries/v1/quality/reviews.json";
        $reviewUrl = $apiUrl . $tsId . "/products/skus/" . HomepageController::strToHex($sku) . "/productreviewstickers/v1/reviews.json";

        $productReviews = json_decode(file_get_contents($reviewUrl), true);
        $productRatingSummary = json_decode(file_get_contents($summaryUrl), true);

        if ($productReviews !== null) {
            $productReviews['response']['data']['product']['showRatings'] = $_GET["showRatings"] ?? "all";
        }

        return $this->render('Product/index.html.twig', [
            'productReviews' => $productReviews['response']['data']['product'],
            'productRatingSummary' => $productRatingSummary['response']['data']['product']
        ]);
    }
    public function icon()
    {
        $path = $this->getParameter('kernel.project_dir') . '/templates/assets/svg';

        $finder = new Finder();

        $templates = [];

        $finder
            ->files()
            ->in($path)
            ->notName(['_base.svg.twig', 'skeleton.stub'])
            ->sortByName();

        foreach ($finder as $file) {
            $templates[] = $file->getFilename();
        }

        return $this->render('Homepage/icon.html.twig', compact("templates"));
    }

    public function components()
    {
        return $this->render('Homepage/components.html.twig');
    }

    public function strToHex($string){
        $hex='';
        for ($i=0; $i < strlen($string); $i++){
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }

    public function sitemap()
    {
        return $this->render('Sitemap/index.html.twig');
    }


}
