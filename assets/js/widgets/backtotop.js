import { addBackToTop } from 'vanilla-back-to-top';

addBackToTop({
    showWhenScrollTopIs: 200,
    scrollDuration: 320,
    cornerOffset: 10,
    diameter: 50
});

const ripples = document.querySelectorAll(".ripple");

if(ripples) {
    let i;

    for (i = 0; i < ripples.length; i++) {
        if ("ontouchstart" in document.documentElement) {
            ripples[i].addEventListener('touchstart', function(event) {
                if (event.target.style.backgroundColor.length <= 0) {
                    event.target.style.backgroundColor = window.getComputedStyle(this).backgroundColor;
                }

            });
        } else {
            ripples[i].addEventListener('mouseover', function(event) {
                    event.target.style.removeProperty("background-color");
            });
        }
    }
}

export default {};
