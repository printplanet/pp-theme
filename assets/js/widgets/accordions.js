import BadgerAccordion from 'badger-accordion';

const accordions = document.querySelectorAll('.js-badger-accordion');

if (accordions) {
    Array.from(accordions).forEach((accordion) => {

        let openHeader = function () {
            let openHeaderIndex = Array.from(
                accordion.querySelectorAll(
                    '.js-badger-accordion-header'
                )
            ).indexOf(
                document.querySelectorAll('input[checked="checked"].js-badger-accordion-header')[0]
            );

            if (openHeaderIndex === -1) return [0];

            return [openHeaderIndex];
        };

        new BadgerAccordion(accordion, { openHeadersOnLoad: openHeader()});
    });
}

export default {};

