let Device = {

    isMobile: function i() {
        return window.matchMedia("(max-width: 991px)").matches
    },
    isRetina: function() {
        return window.matchMedia("(min-resolution: 192dpi)").matches || window.matchMedia("(-webkit-min-device-pixel-ratio: 2)").matches
    }

};

export default Device;