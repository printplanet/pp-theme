import URI from 'urijs';

/*
 * Converts Unicode/ISO encoded with '%20' or '+' as whitespace URLs
 * to ISO 8859 with '+' as whitespace
 */
let Uri = {

    isIso8859 : function(uri) {
        try {
            decodeURIComponent(uri);
        } catch (e) {

            return true;
        }
        return false;
    },

    toIso8859 : function(uri) {
        let query, url;

        URI.unicode();

        if (this.isIso8859(uri)) {
            URI.iso8859();
        }

        url = new URI(uri);

        url = url.escapeQuerySpace(false);

        query = URI.parseQuery(url.query());

        url.setQuery(query);

        if (! this.isIso8859(url.query())) {
            url = url.iso8859();
        }

        uri = url.toString();

        if (/\+/g.test(uri)) {
            uri = uri.replace(/\+/g, '%2B').replace(/\%20/g, '+');
        }

        return uri;
    }

};

export default Uri;
