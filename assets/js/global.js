// Polyfills
require('intersection-observer');
import BadgerAccordion from 'badger-accordion';
import ppUtils from './utils/pp';

// global Vendors
window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.swal = require('sweetalert2');
window.axios = require('axios');

window.PP = ppUtils;
window.BadgerAccordion = BadgerAccordion;

// import Vue from 'vue';
import Accordions from './widgets/accordions';
import BackToTop from './widgets/backtotop';
import Header from './header';
import Presentation from './presentation';

export {
    Accordions, Header, Presentation, BackToTop
}
