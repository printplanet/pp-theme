import BadgerAccordion from 'badger-accordion';

import Pikaday from 'pikaday';

// import { addBackToTop } from 'vanilla-back-to-top';
//
// addBackToTop({
//     showWhenScrollTopIs: 200,
//     scrollDuration: 320,
//     cornerOffset: 10,
//     diameter: 50
// });

const alerts = document.querySelectorAll(".alertClosebtn");

// import Swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js'

import Promise from 'promise-polyfill';

window.swal = Swal;
window.Promise = Promise;

if(alerts) {
    let i;

    for (i = 0; i < alerts.length; i++) {
        alerts[i].onclick = function(){

            let div = this.parentElement;

            div.style.opacity = "0";

            setTimeout(function(){ div.style.display = "none"; }, 600);
        }
    }
}

const accordions = document.querySelectorAll('.js-badger-accordion');

const datepicker = document.querySelector('.pikaday');



if (accordions) {
    Array.from(accordions).forEach((accordion) => {
        let openHeader = function () {
            let openHeaderIndex = Array.from(
                accordion.querySelectorAll(
                    '.js-badger-accordion-header'
                )
            ).indexOf(
                document.querySelectorAll('input[checked="checked"].js-badger-accordion-header')[0]
            );

            // if (openHeaderIndex === -1) return [];

            return [openHeaderIndex];
        };

        const ba = new BadgerAccordion(accordion, { openHeadersOnLoad: openHeader()});
    });
}

require('./bootstrap');

Vue.component('address-view', require('./components/Address.vue'));

var app;

if (document.querySelector('#app')) {
    app = new Vue({
        el: '#app'
    });
}

if (datepicker) {
    let currentTime = new Date();

    let i18n = {
        previousMonth : 'Vormonat',
        nextMonth     : 'Nächster Monat',
        months        : moment.months(),
        weekdays      : moment.weekdays(),
        weekdaysShort : moment.weekdaysShort()
    };

    const picker = new Pikaday({
        field: datepicker,
        format: 'DD.MM.YYYY',
        yearRange: [1900, currentTime.getFullYear()],
        i18n: i18n
    });
}