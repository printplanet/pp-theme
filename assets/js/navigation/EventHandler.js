import $j from "jquery";
import Device from "../utils/device";
import { go } from "../utils/url";
import {
    body,
    globalHeader,
    searchInput,
    globalNavContainer,
    globalNavContent,
    desktopTouchNavBlackout
} from "../DomElements";
import { REQUEST_CLOSE } from "../constants/CustomEvents";

export default class NavigationEventHandler {

    constructor() {
        this.isMenuOpenMobile = false;

        this.isMenuOpenDesktop = false;

        this.delayMenu = 100;

        this.mouseLeaveTimeout = null;

        this.mouseEnterTimeout = null;
    }

    openMenu(element) {
        let isExpandable = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];

        globalNavContent.find('.section-wrapper').removeClass('is-hovering');
        element.addClass('is-hovering');

        if(isExpandable) {
            desktopTouchNavBlackout.addClass('opened');
        }

        if(! element.hasClass('was-open')) {
            element.find('.product-image').each((index, element) => {
                element.prop('src', element.attr("data-src"));
            });
            element.addClass("was-open")
        }

        this.isMenuOpenDesktop = true;
    }

    closeMenu() {
        clearTimeout(this.mouseLeaveTimeout);
        clearTimeout(this.mouseEnterTimeout);
        this.mouseEnterTimeout = null;

        globalNavContent.find('.section-wrapper').removeClass('is-hovering');

        desktopTouchNavBlackout.removeClass('opened');

        this.isMenuOpenDesktop = false;
    }

    touchStartEvent(e) {
        if (! Device.isMobile()) {
            let target = $j(e.target),
                sectionWrapper = target.closest(".section-wrapper"),
                hasAnchor = "a" === e.target.tagName.toLowerCase() || target.closest("a").length;

            if (! $j(sectionWrapper).hasClass('is-hovering') && sectionWrapper.hasClass('section-wrapper')) {
                e.preventDefault();
                let isExpandable = $j(sectionWrapper).hasClass("js-expand-wrapper");
                this.openMenu(sectionWrapper, isExpandable);
            } else {
                if (! hasAnchor) {
                    this.closeMenu();
                }
            }
        }
    }

    handleMouseEnterEvent(e) {
        if (! Device.isMobile() && "touchstart" !== e.type) {
            let target = e.currentTarget;
            clearTimeout(this.mouseLeaveTimeout);
            clearTimeout(this.mouseEnterTimeout);

            if(this.isMenuOpenDesktop) {
                this.openMenu($j(target));
            } else {
                this.mouseEnterTimeout = setTimeout(() => {
                    this.openMenu($j(target));
                }, this.delayMenu);
            }
        }
    }

    handleMouseLeaveEvent() {
        clearTimeout(this.mouseEnterTimeout);
        this.mouseLeaveTimeout = setTimeout(() => {
            this.closeMenu();
        }, this.delayMenu);
    }

    handleClickEvent(e) {
        let target = $j(e.target),
            mobileExpander = target.closest('.js-mobile-expander');

        if (Device.isMobile() && mobileExpander.length) {

            if(this.isMenuOpenMobile) {
                e.preventDefault();
            }
            else {
                this.isMenuOpenMobile = true;

                let trigger = mobileExpander.next('.js-will-expand'),
                    isExpanded = trigger.hasClass('expanded');

                if(isExpanded) {
                    if(target.hasClass('js-mobile-btn-hit-area')) {
                        this.mobileOverlayClose();
                        this.isMenuOpenMobile = false;
                    }
                    else {
                        e.preventDefault();
                        this.mobileMenuItemClose(trigger);
                        mobileExpander.removeClass("show-close-icon");
                    }
                } else {
                    e.preventDefault();

                    let parentNavItems = trigger.parents('.js-expand-wrapper').siblings('.js-expand-wrapper'),
                        height = trigger.outerHeight();

                    trigger
                        .addClass('expanded')
                        .css('height', 0);

                    trigger.animate({
                        height: height
                    }, 250, () => {
                        trigger
                            .css('height', 'auto');

                        this.isMenuOpenMobile = false;
                    });

                    this.mobileMenuItemClose(parentNavItems.find('.expanded'));
                    parentNavItems.find('.show-close-icon').removeClass('show-close-icon');
                    mobileExpander.addClass('show-close-icon');
                }
            }
        }
        else {
            if(target.prop('href')) {
                e.preventDefault();

                this.mobileOverlayClose();

                this.handleMenuLinkEvent(target.prop('href'));
            }
        }
    }

    handleMobileOpenSearchEvent(e) {
        e.preventDefault();

        setTimeout(() => {
            return searchInput.trigger("focus");
        }, 50);
    }

    handleMenuLinkEvent(href) {
        globalHeader.trigger(REQUEST_CLOSE);

        globalNavContainer.find(".show-close-icon").removeClass("show-close-icon");

        globalNavContainer.find('.js-will-expand.expanded').each((key,element) => {
            return this.mobileMenuItemClose($j(element));
        });

        go(href);
    }

    mobileMenuItemClose(element) {
       element.animate({
           height: 0
       }, 250, () => {
           element
               .removeClass('expanded')
               .css('height', 'auto');

           this.isMenuOpenMobile = false;
       });
    }

    mobileOverlayClose() {
        globalHeader.removeClass('mobile-opened');

        globalHeader.addClass('should-animate');

        body.removeClass('mobile-nav-opened');
    }

}
