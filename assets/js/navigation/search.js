import {
    microSearchApp,
    mobileSearchOpener,
    searchBackdrop,
    shopSearch
} from "../DomElements";
import $j from "jquery";
import algoliasearch from 'algoliasearch/lite';
import autocomplete from 'autocomplete.js';
import NavigationEventHandler from "./EventHandler";
import { go } from "../utils/url";

export default class NavigationSearch {

    constructor() {
        this.eventHandler = new NavigationEventHandler();
    }

    init() {

        let client = algoliasearch(window.algolia_app_id, window.algolia_search_key);

        let index = client.initIndex('prod_autocomplete');

        let search = autocomplete(
            '#search',
            {
                templates: {
                  dropdownMenu: '#search-menu-template'
                },
                dropdownMenuContainer: '.header-search .micro-search-app',
                appendTo: '.search-input-col .search-input-suggestion',
//                debug: true,
                hint: false
            },
            [
                {
                    source: autocomplete.sources.hits(index, { hitsPerPage: 10 }),
                    name: 'd1',
                    displayKey: 'term',
                    templates: {
                        suggestion: (suggestion) => {
                            let term = suggestion._highlightResult.term.value,
                                url = this._buildTargetUrl(suggestion.url);
                            return `<a class="highlighted" href="${url}">${term}</a>`;
                        }
                    }
                }
            ]
        );

        search.on('autocomplete:selected', (event, suggestion) => {
            go(this._buildTargetUrl(suggestion.url));
        });

        search.on('autocomplete:opened', () => {
            let popOver = $j('#shop_search .search-popover');

            popOver.parent()
                .css('position', 'static')
            ;

            popOver.show();

            microSearchApp.addClass('is-opened');

            // console.log('dropdown opened');
        });

        search.on('autocomplete:shown', () => {
            // console.log('dropdown shown');
        });

        search.on('autocomplete:empty', () => {
            // console.log('dropdown empty');
        });

        search.on('autocomplete:updated', () => {
            // console.log('dropdown updated');
        });

        search.on('autocomplete:cursorchanged', () => {
            // console.log('dropdown cursorchanged');
        });

        search.on('autocomplete:cursorremoved', () => {
            // console.log('dropdown cursorremoved');
        });

        search.on('autocomplete:autocompleted', () => {
            // console.log('dropdown autocompleted');
        });

        search.on('autocomplete:closed', () => {
            let popOver = $j('#shop_search .search-popover');

            popOver.hide();

            microSearchApp.removeClass('is-opened');

            console.log('dropdown closed');
        });

        mobileSearchOpener.on('click', (e) => {
            this.eventHandler.handleMobileOpenSearchEvent(e);
        });

        shopSearch.find('.backdrop').on('click', (e) => {
           search.autocomplete.close();
        });
    }

    _buildTargetUrl(url) {
        let re = /https:\/\/www.printplanet.dev/gi;

        return url.replace(re, "/produkte");
    }
}
