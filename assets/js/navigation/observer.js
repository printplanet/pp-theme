import { mobileSearchOpener, globalHeader, shopSearch, stickyContent, mainContent, navContainer } from "../DomElements";

export default class NavigationObserver {

    constructor() {
    }

    /**
     *
     */
    init() {
        this.loadMobileSearchObserver();

        this.loadMainNavigationObserver();
    }

    /**
     *
     */
    loadMobileSearchObserver() {
        let observer = new IntersectionObserver(function(entries) {
            entries.forEach(function (entry) {
                const { isIntersecting } = entry;

                if (false === isIntersecting) {
                    mobileSearchOpener.addClass('el-is-exposed');
                    globalHeader.addClass('is-scrolled-past-search');
                    return;
                }

                mobileSearchOpener.removeClass('el-is-exposed');
                globalHeader.removeClass('is-scrolled-past-search');
            });
        }, {
            root: null, // setting root to null sets it to viewport
            rootMargin: '-64px',
            threshold: 0
        });

        let domElements = shopSearch.get();

        domElements.forEach(domElem => {
            observer.observe(domElem);
        });
    }

    loadMainNavigationObserver() {
        let observer = new IntersectionObserver(function(entries) {
            entries.forEach(function (entry) {
                const { isIntersecting } = entry;

                if (false === isIntersecting) {
                    stickyContent.addClass('el-is-snapped');
                    shopSearch.addClass('nav-is-snapped');
                    mainContent.addClass('nav-is-snapped');
                    return;
                }

                stickyContent.removeClass('el-is-snapped');
                shopSearch.removeClass('nav-is-snapped');
                mainContent.removeClass('nav-is-snapped');
            });
        }, {
            threshold: 0
        });

        let domElements = navContainer.get();

        domElements.forEach(domElem => {
            observer.observe(domElem);
        });
    }
}