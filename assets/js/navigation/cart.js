import {
    doc,
    showCartCount,
} from "../DomElements";
import $j from "jquery";
import axios from "axios";
import { CART_REFRESH_COUNT } from "../constants/CustomEvents.js";
import NavigationEventHandler from "./EventHandler";

export default class NavigationCart {

    constructor() {
        this.eventHandler = new NavigationEventHandler();
    }

    init() {
        doc.trigger(CART_REFRESH_COUNT);
    }

    refreshCount() {
        let cartEndpoint = this._getCartRestEndpoint();

        if(cartEndpoint === false) {
            console.debug('Cart ReST endpoint not found. Abort refresh cart count.');

            return;
        }

        axios({'method':'get', 'url': cartEndpoint })
            .then((response) => {
                this._updateDomElements(response.data);
            })
            .catch((error) => {
                console.log(error);
            })
            .finally(function () {
            });
    }

    _getCartRestEndpoint() {
        if(! document.querySelectorAll('meta[name="cart-count-endpoint"]').length) {
            return false;
        }

        return document.querySelectorAll('meta[name="cart-count-endpoint"]')[0].getAttribute("content");
    }

    _updateDomElements(cart) {
        showCartCount.each((k,v) => {
            let ele = $j(v).find('.js-amount');
            if(ele.text().indexOf('(') === -1) {
                ele.text(cart.items_count);
            } else {
                ele.text('(' + cart.items_count + ')');
            }
        });
    }
}
