import $j from "jquery";
import {
    globalHeader,
    doc,
    body,
    globalNavContainer,
    globalNavContent,
    jsClosesDesktopNav
} from "../DomElements";
import NavigationObserver from './observer.js'
import NavigationSearch from './search.js'
import NavigationCart from './cart.js'
import NavigationEventHandler from './EventHandler';
import { REQUEST_CLOSE, CART_REFRESH_COUNT } from '../constants/CustomEvents';

export default class Navigation {

    constructor() {

        this.mobileNavToggle = $j('.js-toggles-mobile-nav');

        this.mobileOverlay = $j('.js-closes-mobile-overlay');

        this.observer = new NavigationObserver();

        this.search = new NavigationSearch();

        this.cart = new NavigationCart();

        this.eventHandler = new NavigationEventHandler();
    }

    /**
     *
     */
    init() {
        this.bindEvents();

        this.observer.init();

        this.search.init();

        this.cart.init();
    }

    bindEvents() {

        this.mobileNavToggle.on('click', function(e) {
            e.preventDefault();

            globalHeader.toggleClass('mobile-opened');

            globalHeader.addClass('should-animate');

            body.addClass('mobile-nav-opened');
        });

        this.mobileOverlay.on('click', (e) => {
            e.preventDefault();

            this.eventHandler.mobileOverlayClose();
        });

        jsClosesDesktopNav.on('click', (e) => {
            e.preventDefault();

            this.eventHandler.closeMenu();
        });

        globalHeader.on('transitionend', () => {
            globalHeader.removeClass('should-animate')
        });

        globalHeader.on(REQUEST_CLOSE, () => {
            return this.eventHandler.closeMenu();
        });

        globalNavContainer.on('click', (e) => {
            this.eventHandler.handleClickEvent(e);
        });

        globalNavContent.on('mouseenter touchstart', '.section-wrapper', (e) => {
            this.eventHandler.handleMouseEnterEvent(e);
        });

        globalNavContent.on('mouseleave', '.section-wrapper', (e) => {
            this.eventHandler.handleMouseLeaveEvent(e);
        });

        globalNavContent.on('touchstart', (e) => {
            return this.eventHandler.touchStartEvent(e);
        });

        doc.on(CART_REFRESH_COUNT, () => {
            this.cart.refreshCount();
        });
    }
}
