import $j from "cash-dom";

export default class PresentationVariants {
    constructor() {
    }

    init () {
        let variants = document.querySelectorAll('.swatch-link');

        Array.from(variants).forEach((variant) => {

            variant.addEventListener('click', function (e) {
                e.preventDefault();

                if (!$j(this).hasClass('active')) {

                    $j(this).parent().children().removeClass('active');
                    $j(this).addClass('active');

                }

            });

        });

    }
}
