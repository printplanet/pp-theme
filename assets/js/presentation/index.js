import PresentationSlideshow from './slideshow';
import PresentationAccordion from './accordion';
import PresentationVariants from './variants';
import ProductReviews from './productReviews';

export default class Presentation {

    constructor() {
        this.slideshow = new PresentationSlideshow();
        this.accordion = new PresentationAccordion();
        this.variants = new PresentationVariants();
        this.productReviews = new ProductReviews();
    }

    init() {

        this.slideshow.init();
        this.accordion.init();
        this.variants.init();
        this.productReviews.init();
    }
}
