import $j from "cash-dom";
import PhotoSwipe from '../../../node_modules/photoswipe';
import PhotoSwipeUI_Default from '../../../node_modules/photoswipe/dist/photoswipe-ui-default.min';
import { Swiper, Navigation, Thumbs, Pagination, Lazy} from 'swiper/js/swiper.esm.js';
Swiper.use([Navigation, Thumbs, Pagination, Lazy]);

export default class PresentationSlideshow {

    constructor() {
        this.galleryTop;
        this.galleryThumb;
    }

    init() {
        this.loadThumbSlideshow();
        this.loadMainSlideshow();
        this.initPhotoSwipeFromDOM('.main-slider');
    }

    loadMainSlideshow(parameters, selector) {
        let _this = this,
            defaultParameters = {
                spaceBetween: 0,
                preloadImages: false,
                lazy: true,
                navigation: {
                    nextEl: '.main-nav-next',
                    prevEl: '.main-nav-prev'
                },
                thumbs: {
                    swiper: _this.galleryThumb
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'fraction',
                },
                breakpoints: {
                    768: {
                        pagination: {
                            el: '.swiper-pagination',
                            type: 'bullets',
                        },
                    },
                },
                on: {
                    init: function () {
                        let element = '.swiper-pagination',
                            type = 'bullets'
                        ;
                        if (window.screen.width < 768) {
                            type = 'fraction';
                        } else {
                            type = 'bullets';
                        }

                        _this.renderPagination(this, element, type)

                    },
                    resize: function() {
                        _this.onResizeSlideshow(this);
                    }
                }
            },
            defaultSelector = '.gallery-top'
        ;

        if (typeof parameters !== 'object') {
            parameters = defaultParameters;
        }
        if (typeof selector !== 'string') {
            selector = defaultSelector;
        }

        this.galleryTop = new Swiper(selector, parameters);
    }

    loadThumbSlideshow(parameters, selector) {
        let defaultParameters = {
            direction: 'vertical',
            spaceBetween: 0,
            slidesPerView: 4,
            preloadImages: false,
            lazy: true,
            navigation: {
                nextEl: '.thumbs-nav-next',
                prevEl: '.thumbs-nav-prev',
            },
        },
            defaultSelector = '.gallery-thumbs'
        ;

        if (typeof parameters !== 'object') {
            parameters = defaultParameters;
        }
        if (typeof selector !== 'string') {
            selector = defaultSelector;
        }

        this.galleryThumb = new Swiper(selector, parameters);
    }

    renderPagination(swiper, paginationElement, paginationType) {
        if (swiper === undefined) {
            return false;
        }

        if (paginationElement === undefined) {
            paginationElement = '.swiper-pagination';
        }

        if (paginationType === undefined) {
            paginationType = 'bullets';
        }

        swiper.params.pagination.el = paginationElement;
        swiper.params.pagination.type = paginationType;

        swiper.pagination.render();
        swiper.pagination.update();
    }

    onResizeSlideshow(slideshow) {
        let render = false,
            element = '.swiper-pagination',
            type = 'bullets'
        ;
        if (window.screen.width < 768 && slideshow.pagination.$el.hasClass('swiper-pagination-bullets')) {
            render = true;
            type = 'fraction';
            slideshow.pagination.$el.removeClass('swiper-pagination-bullets').addClass('swiper-pagination-fraction');
        } else if (window.screen.width >= 768 && slideshow.pagination.$el.hasClass('swiper-pagination-fraction')) {
            slideshow.pagination.$el.removeClass('swiper-pagination-fraction').addClass('swiper-pagination-bullets');
            type = 'bullets';
            render = true;
        }

        if (render) {
            this.renderPagination(slideshow, element, type);
        }
    }


    initPhotoSwipeFromDOM(gallerySelector) {
        let
            slideshow = $j('#slideshow'),
            controlsZoom = $j('.zoom-image', slideshow),
            _this = this
        ;
            // parse slide data (url, title, size ...) from DOM elements
            // (children of gallerySelector)
            let parseThumbnailElements = function(el) {
                let thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item
                ;

                for(let i = 0; i < numNodes; i++) {

                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes
                    if(figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };

                    if(figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML;
                    }

                    if(linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('srcset');
                    }

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            let closest = function closest(el, fn) {
                return el && ( fn(el) ? el : closest(el.parentNode, fn) );
            };

            // triggers when user clicks on thumbnail
            let onThumbnailsClick = function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                let eTarget = e.target || e.srcElement;

                // find root element of slide
                let clickedListItem = closest(eTarget, function(el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if(!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                let clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (let i = 0; i < numChildNodes; i++) {

                    if(childNodes[i].nodeType !== 1) {
                        continue;
                    }

                    if(childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }

                    nodeIndex++;
                }

                if(index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe( index, clickedGallery );
                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            let photoswipeParseHash = function() {
                let hash = window.location.hash.substring(1),
                    params = {};

                if(hash.length < 5) {
                    return params;
                }

                let vars = hash.split('&');
                for (let i = 0; i < vars.length; i++) {
                    if(!vars[i]) {
                        continue;
                    }
                    let pair = vars[i].split('=');
                    if(pair.length < 2) {
                        continue;
                    }
                    params[pair[0]] = pair[1];
                }

                if(params.gid) {
                    params.gid = parseInt(params.gid, 10);
                }

                return params;
            };

            let openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                let pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);

                // define options (if needed)
                options = {

                    // define gallery index (for URL)
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function(index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        let thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                    }

                };

                // PhotoSwipe opened from URL
                if(fromURL) {
                    if(options.galleryPIDs) {
                        // parse real index when custom PIDs are used
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for(let j = 0; j < items.length; j++) {
                            if(items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if( isNaN(options.index) ) {
                    return;
                }

                if(disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                options.timeToIdle = 0;
                options.fullscreenEl = false;
                options.zoomEl = false;
                options.shareEl = false;
                options.clickToCloseNonZoomable = false;

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);

                gallery.listen('unbindEvents', function() {
                    _this.galleryTop.slideTo(gallery.getCurrentIndex());
                    _this.galleryThumb.slideTo(gallery.getCurrentIndex());
                });

                gallery.init();
            };

            // loop through all gallery elements and bind events
            let galleryElements = document.querySelectorAll( gallerySelector );

            for(let i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i+1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            let hashData = photoswipeParseHash();
            if(hashData.pid && hashData.gid) {
                openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
            }

            controlsZoom.on('click', function() {
                openPhotoSwipe(_this.galleryTop.realIndex, _this.galleryTop.$wrapperEl[0]);
            });
    }

}
