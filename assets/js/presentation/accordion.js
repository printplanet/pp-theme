import TabAccordion from '../../../node_modules/storm-tab-accordion';

export default class PresentationAccordion {
    constructor() {
    }

    init () {
        TabAccordion.init('.js-tab-accordion',
            {
                tabClass: '.tabs-tab',
                titleClass: '.accordion-title',
                currentClass: 'active',
                active: 0
            }
        );

    }
}
