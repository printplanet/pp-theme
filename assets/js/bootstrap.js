
window._ = require('lodash');

// import InstantSearch from 'vue-instantsearch';

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');

// require('bootstrap-sass');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');

Vue.config.devtools = true;

//Vue.use(InstantSearch);

/*
let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    if (! window.App.signedIn) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1]);
    }

    return params[0](window.App.user);
};

Vue.prototype.signedIn = window.App.signedIn;
*/

window.moment = require('moment');

window.moment.locale(_.lowerCase(PP.Config.get('client.land')));

window.axios = require('axios');

Vue.prototype.$http = axios;

window.axios.defaults.headers.common = {
//    'X-CSRF-TOKEN': window.App.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

window.Scroll = require('scroll-js');
// window.events = new Vue();

// window.flash = function (message, level = 'success') {
//     window.events.$emit('flash', { message, level });
// };