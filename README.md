# Readme

Dies ist das PrintPlanet Web-Theme mittels Tailwind CSS (https://tailwindcss.com/) und Webpack (Laravel Mix https://laravel.com/docs/5.8/mix) erstellt. 
Die zur Entwicklung nötigen CSS & JS Dateien liegen im assets Ordner und werden nach dem kompilieren (webpack ) in den public/customer Ordner kopiert.

## Installation

### 1. Clone

### 2. Compile the Theme

``npm install``

### 3. Copy the .env

Copy the ``.env.dist`` file to ``.env`` file for development and **change the ENV vars** to your needs.

### 4. NPM Run

``npm run dev``

### 5. Run the Http Server

- Running the Server ``cd pp-theme`` and ``symfony server:start``

## Development

### 1. Watch the changes

``npm run watch``

### 2. Run the Http Server

- Download the Symfony Server Package (https://symfony.com/doc/current/setup/symfony_server.html)
- Running the Server ``cd pp-theme`` and ``symfony server:start``

## Making Packages/Libraries available in the window.document

See ``src/bootstrap.js``
