let mix = require('laravel-mix');

let tailwindcss = require('tailwindcss');

let postCSSCustomProperties = require('postcss-custom-properties');

const path = require('path');

// webpack.config.js
const Dotenv = require('dotenv-webpack');

mix.webpackConfig({
    plugins: [
        new Dotenv()
    ],
    module: {
        rules: [
            {
                test: /\.modernizrrc.js$/,
                use: [ 'modernizr-loader' ]
            },
            {
                test: /\.modernizrrc(\.json)?$/,
                use: [ 'modernizr-loader', 'json-loader' ]
            }
        ]
    },
    resolve: {
        alias: {
            modernizr$: path.resolve(__dirname, ".modernizrrc")
        }
    }
});


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix
    .js('assets/js/app_pf.js', 'public/customer/pf/js/app.js')
    .sass('assets/css/app_pf.scss', 'public/customer/pf/css/app.css')
    .setPublicPath("./")
    .options({
        processCssUrls: false,
        postCss: [
            tailwindcss('./tailwind.config.js'),
            postCSSCustomProperties({
                preserve: false
            })
        ],
      })
;

mix
    .autoload({
        'jquery': ['jQuery', '$'],
    })
    .js('assets/js/app.js', 'public/customer/pp/js/')
    .js('assets/js/global.js', 'public/customer/pp/js/').version().extract(['jquery', 'lodash', 'sweetalert2', 'badger-accordion', 'axios'])
    .sass('assets/css/app.scss', 'public/customer/pp/css/')
    .sass('assets/css/seo.scss', 'public/customer/pp/css/')
    .sass('assets/css/presentation.scss', 'public/customer/pp/css/')
    .setPublicPath("./")
    .options({
        processCssUrls: false,
        postCss: [
            tailwindcss('./tailwind.config.js'),
            postCSSCustomProperties({
                preserve: false
            })
        ],
      })
;

// mix
//     .copy('public/customer/pp/js/app.js', process.env.APP_VOLUME + '/lang/customer/pp_de/js/app.js')
//     .copy('public/customer/pf/js/app.js', process.env.APP_VOLUME + '/lang/customer/pf_de/js/app.js')
//     .copy('public/customer/pf/js/app.js', process.env.APP_VOLUME + '/lang/customer/pf_ro/js/app.js')
//     .copy('public/customer/pf/js/app.js', process.env.APP_VOLUME + '/lang/customer/pf_pl/js/app.js')
//     .copy('public/customer/pp/css/app.css', process.env.APP_VOLUME + '/lang/customer/pp_de/css/app.css')
//     .copy('public/customer/pf/css/app.css', process.env.APP_VOLUME + '/lang/customer/pf_de/css/app.css')
//     .copy('public/customer/pf/css/app.css', process.env.APP_VOLUME + '/lang/customer/pf_ro/css/app.css')
//     .copy('public/customer/pf/css/app.css', process.env.APP_VOLUME + '/lang/customer/pf_pl/css/app.css')
// ;

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   globalVueStyles: file, // Variables file to be imported in every component.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
