var PP = PP || {};

PP.Config = (function () {
    var config = {
        'client.land' : 'DE'
    };
    return {
        get : function(key) {
            return config[key];
        }
    }
})();